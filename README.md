---
lang: zh-tw
---
# ETToday 快速上手 Note
:::info
:bulb: **目的**: 開發流程熟悉筆記
:::
## 目錄

[TOC]
## 基本流程循環分類
 
 1.  ## 環境設定
    1. pod 不用 pod install
    2. swiftLint  
            - 不用pod安裝，安裝於本機系統
            - swiftLint version must 0.39.2
            - brew 只有最新版本，舊版要額外設定.sh，建議用make install指令
    3. Xcode版本 = 12.4
    4. 公司網路名稱，密碼都是公司統編（24328949）
            - ET開頭的
            - APP_TEST
            

 2. ## Git
    1. 不用fork
    2. featur分支需要在PR/路徑下
    3. commit 格式範例，可以參考project commit
       ```javascript=
            TCLOUD-4794 [iOS] 影音播放頁，點擊直立式影片二側，無法叫出Tool Bar,
            RootCase: N/A,
            Solution: N/A,
            Reviewer: Derek,
            Impact：N/A,
            Commit:
                1. 避免 只調isHidden造成alpha不同步
        ```
3. ## Coding style 參考文件
    -  https://github.com/raywenderlich/swift-style-guide
    - 規範參考專案 .swiftlint.yml
4. ## API 相關
    - [API文檔](https://sensengo.sharepoint.com/sites/Enigma/Shared%20Documents/Forms/AllItems.aspx)
        
5. ## Jira
    - issue 記得按start會跟slack ios_pull_request channel連動
    - approve完要自己merge


Q&A
===
- swiftLint不用pod原因
    - **solution:** 在特定電腦有問題，有些無法運作
- 模擬器AuthInputTextField無法輸入
    - **solution:** I/O connect hardware keyboard off
- swiftLint 裝完專案就faile
     - **solution:** 版本必須使用0.39.2




